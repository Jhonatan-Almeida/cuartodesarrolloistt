'use strict';
/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/javascript.js to edit this template
 */

window.addEventListener("load",()=>{
    function cambiarColor(){
     var bg = btn.style.background;
     if(bg === "red"){
         btn.style.background = "green";
         btn.style.color = "white";
         btn.style.padding = "10px";
         btn.style.border = "2px solid #ccc";

     }else{
         btn.style.background = "red";
         btn.style.color = "white";
         btn.style.padding = "20px";
     }
    }


    function calcular(num1, num2){

        var suma = 0;
        suma = parseInt(num1) + parseInt(num2);
        alert("La suma de los dos números es: "+ parseInt(suma));
    }
    var btnCalcular = document.querySelector("#calcular");
    btnCalcular.addEventListener('click',function(){
        var num1 = document.getElementById("numero1").value;
        var num2 = document.getElementById("numero2").value;
        calcular(num1, num2);
    });
    var btn = document.querySelector("#boton");

    btn.addEventListener('click',function(){
        cambiarColor();
    });

    btn.addEventListener('mouseover', function(){
        btn.style.border = "3px solid #FF5733";
        btn.style.cursor = "pointer";
    });

    btn.addEventListener('mouseout', function(){
        btn.style.border = "3px solid #f5f5f5";
    });

    //focus
    var txt = document.getElementById("campo_nombre");
    txt.addEventListener('focus', function(){
        console.log ("has ganado el foco");
    });

    //blur
    txt.addEventListener('blur', function(){
        console.log ("has salido del focus");
    });
    //keydown
    txt.addEventListener('keydown', function(event){
        console.log ("has presionado la tecla : ",String.fromCharCode(event.keyCode));
    });

    //keypress

    txt.addEventListener('keypress', function(event){
        console.log ("has tecleado la letra o numero : ",String.fromCharCode(event.keyCode));
    });

    //keyup

    txt.addEventListener('keyup', function(event){
        console.log ("Has soltado la tecla : ",String.fromCharCode(event.keyCode));
    }); 
});