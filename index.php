<?php
    include './include/cabecera.php';
    include './include/lateral.php';
?>
        
            <div id="principal">
                <h1>Entradas</h1>
                <article class="entrada">
                    <a href="#">
                        <h2>
                            Blog de Desarrollo
                        </h2>
                        <span class="fecha">Educación | 2023-11-21</span>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    </a>
                    
                </article>
            </div>
        </div><!-- Fin del Contenedor -->
       
<?php
    include './include/pie.php';

?>
